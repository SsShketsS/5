def celToKel(cel):
    kel = cel + 273
    return int(kel)

def kelToCel(kel):
    cel = kel - 273
    return int(cel)

def fahToCel(fah):
    cel = 5.0 * (fah - 32) / 9
    return int(cel)

def celToFah(cel):
    fah = (9 / 5.0 * cel) + 32
    return int(fah)

def fahToKel(fah):
    cel = fahToCel(fah)
    kel = celToKel(cel)
    return int(kel)

def kelToFah(kel):
    cel = kelToCel(kel)
    fah = celToFah(cel)
    return int(fah)

def preobrazovanie(value, first_name, second_name):
    if first_name == 'cel':
        if second_name == 'fah':
            result = celToFah(value)
        if second_name == 'kel':
            result = celToKel(value)
    elif first_name == 'kel':
        if second_name == 'fah':
            result = kelToFah(value)
        if second_name == 'cel':
            result = kelToCel(value)
    elif first_name == 'fah':
        if second_name == 'cel':
            result = fahToCel(value)
        if second_name == 'kel':
            result = fahToKel(value)
    else: return "Error"

    with open('test.txt', 'a') as the_file:
        the_file.write(f'{value} {first_name} = {result} {second_name}\n')

    return result
