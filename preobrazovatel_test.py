from preobrazovatel import preobrazovanie

def test_FahToCel():
    assert preobrazovanie(25, 'fah', 'cel') == -3

def test_CelToKel():
    assert preobrazovanie(25, 'cel', 'kel') == 298

def test_KelToFah():
    assert preobrazovanie(25, 'kel', 'fah') == -414

def test_CelToFah():
    assert preobrazovanie(25, 'cel', 'fah') == 77

def test_FahToKel():
    assert preobrazovanie(25, 'fah', 'kel') == 270

def test_KelToCel():
    assert preobrazovanie(25, 'kel', 'cel') == -248
def tesr_error():
    assert preobrazovanie('kel', 'kel', 'cel') == "Error"